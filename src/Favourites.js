import React, { useState } from 'react'
import {Link} from 'react-router-dom';
const Favourites=({value})=>{
const [key, setkey] = useState(Math.random());

    const favList= value.map(element=>{
        
        if(element.isFavourite===true){
        return <div key={element.id}>
                <h4>{element.name}</h4>
                <button className="negative ui button" 
                id={key} value={element.name} 
                onClick={()=>{element.isFavourite=false;setkey(Math.random())}}> I don't like {element.name} anymore</button><br/><br/>
              </div>
        }
    
    })
    
return(
<div className="ui container">
<h1>Your Favourite Planets are:</h1><br/><br/>
{favList}
<br/><br/>
<Link to="/">
<button className="ui secondary button">
  Add Another Planet to my Favourites
</button>
</Link>
</div>
);
}

export default Favourites;
