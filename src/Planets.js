import React, { useState } from 'react';
import {Link} from 'react-router-dom';

const Planets=({planets})=>{
  const [key, setkey] = useState(Math.random())
  const list= planets.map(element=>{
    
      return(
        <div key={element.id}>
        <input type="checkbox" key={key} name={element.name} onChange={event=>{
            setkey(Math.random());
            element.isFavourite=event.target.checked}}  checked={element.isFavourite} /> 
        <label id={element.id}>  {element.name}</label> <br/><br/>
        </div>
      )
    })
   
    return (
      <div className="ui container">
        <form onSubmit={(event)=>{
        event.preventDefault();
      }}>
        <h2>Choose your favourite Planets:</h2><br/><br/>
        {list} 
      <Link to="/favourites">
        <button className="ui secondary button" type="submit">Submit</button>
        </Link>
        </form>
      </div>
    )
}
export default Planets